import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:makanan_list/detail_page.dart';
import 'package:makanan_list/makanan.dart';
import 'package:makanan_list/style.dart';

class item extends StatelessWidget {
  const item({
    super.key,
    required this.Menu,
  });

  final Makanan Menu;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => DetailPage(maknan: Menu)));
      },
      child: Container(
        margin: EdgeInsets.all(8),
        height: 100,
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(8)),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(255, 118, 188, 188),
                  offset: Offset(1, 2),
                  blurRadius: 6)
            ]),
        child: Row(
          children: [
            gambar(),
            SizedBox(
              width: 10,
            ),
            deskripsi_teks(),
            Icon(
              Icons.food_bank_rounded,
              color: IconColor,
            )
          ],
        ),
      ),
    );
  }

  ClipRRect gambar() {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(8)),
      child: Image.asset(
        Menu.gambarUtama,
        height: 75,
        width: 85,
        fit: BoxFit.cover,
      ),
    );
  }

  Expanded deskripsi_teks() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            Menu.nama,
            style: headerH1,
          ),
          Text(
            Menu.deskripsi,
            style: TextStyle(color: Colors.black38),
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            Menu.harga,
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
