import 'package:flutter/material.dart';
import 'package:makanan_list/makanan.dart';
import 'package:makanan_list/style.dart';

class DetailPage extends StatelessWidget {
  final Makanan maknan;

  DetailPage({super.key, required this.maknan});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: pageBgColor,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Stack(
                children: [
                  Image.asset(
                    maknan.gambarUtama,
                    scale: 0.5,
                  ),
                  Container(
                    margin: EdgeInsets.all(25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [ButtonBack(), ButtonLike()],
                    ),
                  )
                ],
              ),
              Container(
                color: headBgColor,
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Text(
                  maknan.nama,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 32,
                      color: Colors.white),
                ),
              ),
              SizedBox(
                height: 13,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  atributesIcon(Icons.access_time_filled, maknan.waktuBuka),
                  atributesIcon(Icons.local_fire_department, maknan.kalori),
                  atributesIcon(Icons.monetization_on, maknan.harga),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                child: Text(
                  maknan.detail,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16),
                ),
              ),
              ListGambar(maknan: maknan),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  "Komposisi",
                  style: headerH1,
                ),
              ),
              SizedBox(
                height: 100,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: maknan.bahan.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.only(right: 10),
                      width: 120,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Column(
                        children: [
                          Image.asset(
                            maknan.bahan[index].values.first,
                            width: 52,
                          ),
                          Text(maknan.bahan[index].keys.first)
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Column atributesIcon(IconData icon, String teks) {
    return Column(
      children: [
        Icon(Icons.access_time_filled, color: IconColor),
        Text(
          teks,
          style: TextStyle(
            fontSize: 11,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }
}

class ListGambar extends StatelessWidget {
  const ListGambar({
    super.key,
    required this.maknan,
  });

  final Makanan maknan;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: maknan.gambarLain.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(maknan.gambarLain[index])),
          );
        },
      ),
    );
  }
}

class ButtonBack extends StatelessWidget {
  const ButtonBack({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Colors.grey,
      child: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back_ios_rounded,
          color: Colors.white,
        ),
      ),
    );
  }
}

class ButtonLike extends StatefulWidget {
  const ButtonLike({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ButtonLike();
}

class _ButtonLike extends State<ButtonLike> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Colors.grey,
      child: IconButton(
        onPressed: () {
          setState(() {
            isSelected = !isSelected;
          });
        },
        icon: Icon(
          isSelected ? Icons.favorite : Icons.favorite_outline,
          color: Colors.red,
        ),
      ),
    );
  }
}
